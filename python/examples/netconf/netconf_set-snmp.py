#!/usr/bin/env python3

from ncclient import manager
from ncclient.xml_ import *
import docker


# Functions
def get_docker_ip(container_name):
    client = docker.APIClient(base_url='unix://var/run/docker.sock')
    return client.inspect_container(container_name)['NetworkSettings']['IPAddress']


def commit_configuration(connection, configuration):
    print('*' * 50)
    print('Committing configuration')
    print('*' * 50)

    print('- Locking configuration')
    lock = connection.lock()

    print('- Loading configuration')
    send_config = connection.load_configuration(action='set', config=configuration)
    print(send_config.tostring)

    print('- Validating loaded configuration')
    check_config = connection.validate()
    print(check_config.tostring)

    print('- Comparing loaded configuration')
    compare_config = connection.compare_configuration()
    print(compare_config.tostring)

    print('- Committing configuration')
    commit_config = connection.commit()
    print(commit_config.tostring)

    print('- Unlocking configuration')
    unlock = connection.unlock()
    print(unlock.tostring)


# Define Variables
vrnetlab_username = 'vrnetlab'
vrnetlab_password = 'VR-netlab9'

vmx = {
    'host': get_docker_ip('vmx'),
    'port': 22,
    'username': vrnetlab_username,
    'password': vrnetlab_password,
    'timeout': 10,
    'device_params': {'name': 'junos'},
    'hostkey_verify': False
}

# Setup connection
netconf_connection = manager.connect(**vmx)

# Commit configuration
snmp_community_and_password = 'vrnetlab'
config = []
config.append('delete snmp')
# SNMPv2
# config.append('set snmp community ' + snmp_community_and_password)
# SNMPv3
config.append('set snmp v3 usm local-engine user NOAUTH authentication-none')
config.append('set snmp v3 usm local-engine user AUTHNOPRIV authentication-sha authentication-password ' + snmp_community_and_password)
config.append('set snmp v3 usm local-engine user AUTHNOPRIV privacy-none')
config.append('set snmp v3 usm local-engine user AUTHPRIV authentication-sha authentication-password ' + snmp_community_and_password)
config.append('set snmp v3 usm local-engine user AUTHPRIV privacy-aes128 privacy-password ' + snmp_community_and_password)
config.append('set snmp v3 vacm security-to-group security-model v2c security-name NOSNMPV3 group SNMPV3GROUP')
config.append('set snmp v3 vacm security-to-group security-model usm security-name NOAUTH group SNMPV3GROUP')
config.append('set snmp v3 vacm security-to-group security-model usm security-name AUTHNOPRIV group SNMPV3GROUP')
config.append('set snmp v3 vacm security-to-group security-model usm security-name AUTHPRIV group SNMPV3GROUP')
config.append('set snmp view SNMPVIEW oid .1 include')  # All OIDs
config.append('set snmp v3 vacm access group SNMPV3GROUP default-context-prefix security-model any security-level none read-view SNMPVIEW')
config.append('set snmp v3 vacm access group SNMPV3GROUP default-context-prefix security-model any security-level authentication read-view SNMPVIEW')
config.append('set snmp v3 vacm access group SNMPV3GROUP default-context-prefix security-model any security-level privacy read-view SNMPVIEW')
config.append('set snmp v3 snmp-community SNMPV3COMMUNITY community-name ' + snmp_community_and_password)
config.append('set snmp v3 snmp-community SNMPV3COMMUNITY security-name NOSNMPV3')

commit_configuration(netconf_connection, config)
