#!/usr/bin/env python3

from ncclient import manager
from ncclient.xml_ import *
import docker


# Functions
def get_docker_ip(container_name):
    client = docker.APIClient(base_url='unix://var/run/docker.sock')
    return client.inspect_container(container_name)['NetworkSettings']['IPAddress']


def run_show_command(connection, show_command):
    print('*' * 50)
    print(show_command)
    print('*' * 50)
    result = connection.command(show_command, format='text')
    print(result.xpath('output')[0].text)


def compare_rollbacks(connection, rollback_number):
    print('*' * 50)
    print('Comparing current configuration to rollback ' + str(rollback_number))
    print('*' * 50)
    compare_config = connection.compare_configuration(rollback=rollback_number)
    print(compare_config.tostring)


def commit_configuration(connection, configuration):
    print('*' * 50)
    print('Committing configuration')
    print('*' * 50)

    print('- Locking configuration')
    lock = connection.lock()

    print('- Loading configuration')
    send_config = connection.load_configuration(action='set', config=configuration)
    print(send_config.tostring)

    print('- Validating loaded configuration')
    check_config = connection.validate()
    print(check_config.tostring)

    print('- Comparing loaded configuration')
    compare_config = connection.compare_configuration()
    print(compare_config.tostring)

    print('- Committing configuration')
    commit_config = connection.commit()
    print(commit_config.tostring)

    print('- Unlocking configuration')
    unlock = connection.unlock()
    print(unlock.tostring)


def get_configuration(connection, hierarchy=''):
    print('*' * 50)
    print('Getting configuration')
    print('*' * 50)

    print('- Getting configuration')
    full_configuration = connection.get_config('running')

    if hierarchy:
        print('- Showing ' + hierarchy + ' hierarchy')
        output = full_configuration.xpath('data/configuration/' + hierarchy)[0]
        print(to_xml(output))

        # To get DNS servers
        # hierarchy = 'system/name-server/name'
        # for item in output = full_configuration.xpath('data/configuration/' + hierarchy):
        #   print(to_xml(item))


# Define Variables
vrnetlab_username = 'vrnetlab'
vrnetlab_password = 'VR-netlab9'

vmx = {
    'host': get_docker_ip('vmx'),
    'port': 22,
    'username': vrnetlab_username,
    'password': vrnetlab_password,
    'timeout': 10,
    'device_params': {'name': 'junos'},
    'hostkey_verify': False
}

# Setup connection
netconf_connection = manager.connect(**vmx)

# Run a show command
run_show_command(netconf_connection, 'show version')

# Compare rollbacks
compare_rollbacks(netconf_connection, 2)

# Commit configuration
config = []
config.append('set host-name foo')
config.append('set host-name foo')

commit_configuration(netconf_connection, config)

# Get configuration
config_hierarchy = 'system'
get_configuration(netconf_connection, config_hierarchy)
