#!/usr/bin/env python3

import docker
from jnpr.junos import Device
from jnpr.junos.utils.config import Config
from jnpr.junos.exception import *
from lxml import etree


# Functions
def get_docker_ip(container_name):
    client = docker.APIClient(base_url='unix://var/run/docker.sock')
    return client.inspect_container(container_name)['NetworkSettings']['IPAddress']


# Define Variables
vrnetlab_username = 'vrnetlab'
vrnetlab_password = 'VR-netlab9'

vmx = {
    'host': get_docker_ip('vmx'),
    'port': 22,
    'user': vrnetlab_username,
    'passwd': vrnetlab_password,
}

# Setup connection and get facts
with Device(**vmx) as pyez_connection:
    print('*' * 50)
    print('Host facts:')
    print('*' * 50)
    print(pyez_connection.facts)

# Perform RPC call, as follows:
"""
show route table inet.0 | display xml rpc
 <rpc-reply xmlns:junos="http://xml.juniper.net/junos/17.2R1/junos">
     <rpc>
         <get-route-information>
                 <table>inet.0</table>
         </get-route-information>
     </rpc>
     <cli>
         <banner></banner>
     </cli>
 </rpc-reply>
"""
with Device(**vmx) as pyez_connection:
    print('*' * 50)
    print('Route Information:')
    print('*' * 50)
    route_lxml_element = pyez_connection.rpc.get_route_information(table="inet.0")
    list_of_routes = route_lxml_element.findall('.//rt')
    for route in list_of_routes:
        print("Route: {} Protocol: {}".format(route.findtext('rt-destination').strip(),
                                              route.findtext('rt-entry/protocol-name').strip()))

# Get configuration, as follows:
"""
show configuration interfaces | display xml
<rpc-reply xmlns:junos="http://xml.juniper.net/junos/17.2R1/junos">
    <configuration junos:commit-seconds="1569487654" junos:commit-localtime="2019-09-26 08:47:34 UTC" junos:commit-user="root">
            <interfaces>
                <interface>
                    <name>fxp0</name>
                    <unit>
                        <name>0</name>
                        <family>
                            <inet>
                                <address>
                                    <name>10.0.0.15/24</name>
                                </address>
                            </inet>
                        </family>
                    </unit>
                </interface>
            </interfaces>
    </configuration>
    <cli>
        <banner></banner>
    </cli>
</rpc-reply>
"""
with Device(**vmx) as pyez_connection:
    print('*' * 50)
    print('Interface Configuration:')
    print('*' * 50)
    interface_configuration = pyez_connection.rpc.get_config(filter_xml=etree.XML('<configuration><interfaces/></configuration>'))
    print(etree.tostring(interface_configuration))

# Get all configuration:
with Device(**vmx) as pyez_connection:
    print('*' * 50)
    print('All Configuration:')
    print('*' * 50)
    interface_configuration = pyez_connection.rpc.get_config()
    print(etree.tostring(interface_configuration))

# Unstructured Configuration Changes
with Device(**vmx) as pyez_connection:
    print('*' * 50)
    print('Setting configuration:')
    print('*' * 50)

    config = "set system host-name monty"

    pyez_connection_configuration = Config(pyez_connection)
    pyez_connection_configuration.lock()
    pyez_connection_configuration.load(config, format='set')
    pyez_connection_configuration.pdiff()  # Prints the diff
    if pyez_connection_configuration.commit_check():
        pyez_connection_configuration.commit()
    else:
        pyez_connection_configuration.rollback()
    pyez_connection_configuration.unlock()

# Exception Examples
try:
    with Device(**vmx) as pyez_connection:
        print('*' * 50)
        print('Host facts with exceptions:')
        print('*' * 50)
        print(pyez_connection.facts)
except ConnectAuthError:
    print("Authentication Error!")
except ConnectTimoutError:
    print("Connection Timed Out!")
except ConnectError as error:
    print("Other connection error: %s!" % str(error))
except ConfigLoadError:
    print("Failed when loading configuration!")
except Exception as error:
    print("Other exception: %s" % str(error))
