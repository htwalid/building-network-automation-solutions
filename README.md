# Building Network Automation Solutions - Exercises

A repository for practice, labbing and submitting exercises completed as a part of [ipSpace's Building Network Automation Solutions](https://my.ipspace.net/bin/list?id=NetAutSol) course.
